self.addEventListener('message', function(e){
	
	var randomArray = [];
	var length = e.data;

	for(var i=0; i<length; i++){
		randomArray.push(Math.random());
	}

	var sortedArray = randomArray.sort();

	self.postMessage(sortedArray);

}, false);