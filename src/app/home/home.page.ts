import { Component } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public items: any[] = new Array(50);

  constructor(public dataService: DataService){

  }

  startSort(){
    this.dataService.doSortWithWorker();
  }

}
